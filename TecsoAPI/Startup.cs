using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using TecsoData;
using TecsoInfrastructure.Mapping;
using Swashbuckle.AspNetCore;
using Microsoft.OpenApi.Models;
using AutoMapper;
using FluentValidation.AspNetCore;
using TecsoInfrastructure.Validation;
using FluentValidation;
using TecsoContract.Alumno;
using TecsoContract;
using TecsoInfrastructure.ExceptionHandling;
using System.IO;
using TecsoInfrastructure.Log;
using TecsoInfrastructure.AppSettings;
using System.Globalization;
using Microsoft.AspNetCore.Localization;

namespace TecsoAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Tecso API", Version = "v1" });
            });

            services.AddDbContext<TecsoDbContext>();

            var configuration = new ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", true)
    .Build();

            var connection = configuration.GetConnectionString("SqliteConnection");
            var loggingAppSetting = configuration.GetSection("LogPath");
            AppSettingsProvider.SqliteConnection = connection;
            AppSettingsProvider.PathLogFolder = loggingAppSetting.Value;

            services.AddAutoMapper(typeof(MappingProfile));

            services.AddMvc(opt =>
            {
                opt.Filters.Add(typeof(ValidationFilter));

            }).AddFluentValidation(mvcConfiguration => mvcConfiguration.RegisterValidatorsFromAssemblyContaining<DtoEntity>());
        }

        // This method gets called by the runtime. Use this metshod to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();

            var cultureInfo = new CultureInfo("es-AR") { NumberFormat = { CurrencySymbol = "$" } };
            var defaultDateCulture = "es-AR";
            var ci = new CultureInfo(defaultDateCulture);
            ci.NumberFormat.NumberDecimalSeparator = ",";
            ci.NumberFormat.CurrencyDecimalSeparator = ",";

            // Configure the Localization middleware
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(ci),
                SupportedCultures = new List<CultureInfo>
                {
                    ci,
                },
                SupportedUICultures = new List<CultureInfo>
                {
                    ci,
                }
            });

            app.UseSwaggerUI(c =>
        {
            c.SwaggerEndpoint("/swagger/v1/swagger.json", "Tecso API");
        });
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseMiddleware<ExceptionHandlingMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
