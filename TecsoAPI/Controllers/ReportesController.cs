﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TecsoContract.Alumno;
using TecsoData;

namespace TecsoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportesController : ControllerBase
    {
        private readonly TecsoDbContext _tecsoDb;
        private readonly IMapper _mapper;


        public ReportesController(TecsoDbContext tecsoDb, IMapper mapper)
        {
            _tecsoDb = tecsoDb;
            _mapper = mapper;
        }


        // get: api/Reportes
        [HttpGet("alumnos/aprobados/{idCurso}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public ActionResult<IList<AlumnoDto>> get(int idCurso)
        {
            var curso = _tecsoDb.Cursos.FirstOrDefault(x => x.Id == idCurso);
            if (curso == null)
                return NotFound("No existe el curso");

            var alumnos = curso.Alumnos.Where(x => x.EstadoCursada.Nombre == "Aprobado").Select(x => x.Alumno);

            return Ok(_mapper.Map(alumnos, new List<AlumnoDto>()));
        }

    }
}