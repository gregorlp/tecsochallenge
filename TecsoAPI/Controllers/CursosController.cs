﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using TecsoContract.Alumno;
using TecsoContract.Curso;
using TecsoData;
using TecsoInfrastructure.Log;
using TecsoModel.Model;

namespace TecsoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CursosController : ControllerBase
    {
        private readonly TecsoDbContext _tecsoDb;
        private readonly IMapper _mapper;


        public CursosController(TecsoDbContext tecsoDb, IMapper mapper)
        {
            _tecsoDb = tecsoDb;
            _mapper = mapper;
        }
        #region Methods

        #region Todos los Cursos
        // GET: api/Cursos
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public ActionResult<IList<CursoDto>> Get()
        {
            var cursos = _tecsoDb.Cursos.ToList();

            return Ok(_mapper.Map(cursos, new List<CursoDto>()));
        }

        #endregion

        #region Un Curso Especifico
        // GET: api/Cursos/5
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public ActionResult<CursoDto> Get(int id)
        {
            if (id == 0)
                return BadRequest();
            var curso = _tecsoDb.Cursos.FirstOrDefault(x => x.Id == id);

            if (curso == null)
                return NotFound();

            return Ok(_mapper.Map(curso, new CursoDto()));
        }

        #endregion

        #region Create Curso

        // POST: api/Cursos
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public ActionResult Post([FromBody] CreateCursoRequestDto createCurso)
        {
            var curso = _tecsoDb.Cursos.FirstOrDefault(x => x.IdMateria == createCurso.IdMateria && x.Semestre == createCurso.Semestre && createCurso.Año == x.Año);
            if (curso != null)
                return BadRequest("Ya existe el Curso");
            var materia = _tecsoDb.Materias.FirstOrDefault(x => x.Id == createCurso.IdMateria);
            if (materia == null)
                return BadRequest("No Existe la Materia");

            var newCurso = _mapper.Map(createCurso, new Curso());

            _tecsoDb.Cursos.Add(newCurso);

            _tecsoDb.SaveChanges();

            TecsoLogger.LogMessage("Se agrego un curso", Tipos.NivelLog.Informativo);

            return Ok("Se agrego el Curso");
        }
        #endregion

        #region Aprobar Cursada a Alumno

        // POST: api/Cursos/5/aprobar/1
        [HttpPost("{idCurso}/aprobar/{idAlumno}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public ActionResult Post(int idCurso, int idAlumno)
        {
            var curso = _tecsoDb.Cursos.FirstOrDefault(x => x.Id == idCurso);
            if (curso == null)
                return BadRequest("No Existe el Curso");

            var alumno = _tecsoDb.Alumnos.FirstOrDefault(x => x.Id == idAlumno);
            if (alumno == null)
                return BadRequest("No Existe el Alumno");

            var inscripcion = _tecsoDb.Inscripciones.FirstOrDefault(x => x.IdAlumno == idAlumno && x.IdCurso == idCurso);
            if (inscripcion == null)
                return NotFound("El Alumno no esta inscripto en la Materia");
            if (inscripcion.EstadoCursada.Descripcion == "Aprobado")
                return BadRequest("El Alumno ya aprobo la materia");

            var estadoAprobado = _tecsoDb.EstadosCursada.FirstOrDefault(x => x.Nombre == "Aprobado");

            if (estadoAprobado == null)
                return BadRequest("No existe el Estado Aprobado");

            inscripcion.IdEstadoCursada = estadoAprobado.Id;

            _tecsoDb.Inscripciones.Update(inscripcion);

            _tecsoDb.SaveChanges();

            TecsoLogger.LogMessage("Se aprobo la cursada de un alumno", Tipos.NivelLog.Informativo);

            return Ok("Se Aprobo al Alumno");
        }
        #endregion

        #region Eliminar un Curso
        // DELETE: api/Cursos/5
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public ActionResult Delete(int id)
        {
            var curso = _tecsoDb.Cursos.FirstOrDefault(x => x.Id == id);

            if (curso == null)
                return NotFound();

            if (_tecsoDb.Inscripciones.Any(x => x.IdCurso == id))
                return BadRequest("No se puede borrar el curso por que existen inscripciones");

            _tecsoDb.Cursos.Remove(curso);

            _tecsoDb.SaveChanges();

            TecsoLogger.LogMessage("Se elimino un curso", Tipos.NivelLog.Informativo);

            return Ok("Se elimino el curso correctamente");
        }
        #endregion

        #endregion
    }
}
