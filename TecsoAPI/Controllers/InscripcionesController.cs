﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TecsoContract.Alumno;
using TecsoContract.Inscripcion;
using TecsoData;
using TecsoInfrastructure.Log;
using TecsoModel.Model;

namespace TecsoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InscripcionesController : ControllerBase
    {
        private readonly TecsoDbContext _tecsoDb;
        private readonly IMapper _mapper;


        public InscripcionesController(TecsoDbContext tecsoDb, IMapper mapper)
        {
            _tecsoDb = tecsoDb;
            _mapper = mapper;
        }

        #region Inscripciones

        #region Todas las inscripciones
        // GET: api/Inscripcion
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public ActionResult<IList<AlumnoCursoDto>> Get()
        {
            var inscripciones = _tecsoDb.Inscripciones.ToList();

            return _mapper.Map(inscripciones, new List<AlumnoCursoDto>());
        }

        #endregion

        #region Todas las inscripciones de un curso
        // GET: api/Inscripcion/Curso/5
        [HttpGet("Curso/{idCurso}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public ActionResult<AlumnoDto> GetAlumnosInscriptos(int idCurso)
        {
            var curso = _tecsoDb.Cursos.FirstOrDefault(x => x.Id == idCurso);
            if (curso == null)
                return NotFound("No existe el curso");

            var alumnos = curso.Alumnos.Select(x => x.Alumno);

            return Ok(_mapper.Map(alumnos, new List<AlumnoDto>()));
        }

        #endregion

        #region Create Inscripcion
        // POST: api/Inscripcion
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public ActionResult Post([FromBody] CreateInscripcionRequestDto inscrpcion)
        {
            var curso = _tecsoDb.Cursos.FirstOrDefault(c => c.Id == inscrpcion.IdCurso);
            if (curso == null)
                return NotFound("No Existe el curso");
            var alumno = _tecsoDb.Alumnos.FirstOrDefault(a => a.Id == inscrpcion.IdAlumno);
            if (alumno == null)
                return NotFound("No Existe el alumno");
            var alumnoCurso = _tecsoDb.Inscripciones.FirstOrDefault(i => i.IdCurso == inscrpcion.IdCurso && i.IdAlumno == inscrpcion.IdAlumno);
            if (alumnoCurso != null)
                return BadRequest("El alumno ya se encuentra inscripto al curso");
            var newInscripcion = _mapper.Map(inscrpcion, new AlumnoCurso());

            var estadoEnCurso = _tecsoDb.EstadosCursada.FirstOrDefault(x => x.Nombre == "En Curso");

            if (estadoEnCurso == null)
                return BadRequest("No existe el estado de cursada: En Curso");

            newInscripcion.IdEstadoCursada = estadoEnCurso.Id;

            _tecsoDb.Inscripciones.Add(newInscripcion);

            _tecsoDb.SaveChanges();

            TecsoLogger.LogMessage("Se inscribio un alumno al curso", Tipos.NivelLog.Informativo);

            return Ok("Se inscribio al alumno");

        }

        #endregion

        #region Eliminar inscripcion de un alumno a un curso
        // DELETE: api/Inscripcion/5
        [HttpDelete("{idCurso}/{idAlumno}")]
        public ActionResult Delete(int idCurso, int idAlumno)
        {
            if (!_tecsoDb.Cursos.Any(x => x.Id == idCurso))
                return BadRequest("No Existe el Curso");
            if (!_tecsoDb.Alumnos.Any(x => x.Id == idAlumno))
                return BadRequest("No Existe el Alumno");
            var inscripcion = _tecsoDb.Inscripciones.FirstOrDefault(x => x.IdAlumno == idAlumno && x.IdCurso == idCurso);
            if (inscripcion == null)
                return NotFound("No Existe la inscripción");

            _tecsoDb.Inscripciones.Remove(inscripcion);
            _tecsoDb.SaveChanges();

            TecsoLogger.LogMessage("Se elimino la inscripcion de un alumno", Tipos.NivelLog.Informativo);

            return Ok("Inscripcion eliminada");
        }

        #endregion

        #endregion


        #region Estados de Cursada

        #region Todos los Estados de Cursada disponibles
        // POST: api/Inscripcion/EstadosCursada
        [HttpGet("EstadosCursada")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public ActionResult<IList<EstadoCursadaDto>> GetEstadosCursada()
        {
            var estadosCursada = _tecsoDb.EstadosCursada.ToList();

            return Ok(_mapper.Map(estadosCursada, new List<EstadoCursadaDto>()));
        }

        #endregion

        #region Crear un Estado de Cursada
        // POST: api/Inscripcion/CreateEstadoCursada
        [HttpPost("CreateEstadoCursada")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public ActionResult CreateEstadoCursada([FromBody] CreateEstadoCursadaRequestDto createEstadoCursadaRequest)
        {
            var estadoCursada = _tecsoDb.EstadosCursada.Any(x => x.Nombre == createEstadoCursadaRequest.Nombre);

            if (estadoCursada)
                return BadRequest("Ya Existe un Estado de cursada con el mismo nombre");


            _tecsoDb.EstadosCursada.Add(new EstadoCursada() { Nombre = createEstadoCursadaRequest.Nombre, Descripcion = createEstadoCursadaRequest.Descripcion });

            _tecsoDb.SaveChanges();

            TecsoLogger.LogMessage("Se creo un Estado de Cursada", Tipos.NivelLog.Informativo);

            return Ok("Se agrego el Estado de curso");

        }
        #endregion

        #endregion

    }
}
