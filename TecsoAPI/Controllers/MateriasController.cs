﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TecsoContract.Alumno;
using TecsoContract.Materia;
using TecsoData;
using TecsoInfrastructure.Log;
using TecsoModel.Model;

namespace TecsoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MateriasController : ControllerBase
    {
        private readonly TecsoDbContext _tecsoDb;
        private readonly IMapper _mapper;


        public MateriasController(TecsoDbContext tecsoDb, IMapper mapper)
        {
            _tecsoDb = tecsoDb;
            _mapper = mapper;
        }

        #region Methods

        #region Todas las Materias
        // GET: api/Materia
        [HttpGet]
        //[ProducesResponseType(200)]
        //[ProducesResponseType(500)]
        public ActionResult<IList<MateriaDto>> Get()
        {
            var materias = _tecsoDb.Materias.ToList();

            return _mapper.Map(materias, new List<MateriaDto>());
        }
        #endregion

        #region Una Materia Especifica
        // GET: api/Materia/5
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public ActionResult<MateriaDto> Get(int id)
        {
            var materia = _tecsoDb.Materias.FirstOrDefault(x => x.Id == id);

            if (materia != null)
                return Ok(_mapper.Map(materia, new MateriaDto()));

            return NotFound();
        }
        #endregion

        #region Crear una Materia
        // POST: api/Materia
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public ActionResult Post([FromBody] CreateOrEditMateriaRequestDto materiaDto)
        {
            if (_tecsoDb.Materias.Any(x => x.Nombre == materiaDto.Nombre))
                return BadRequest("Ya Existe una materia con el mismo Nombre");

            var materia = _mapper.Map(materiaDto, new Materia());

            _tecsoDb.Materias.Add(materia);

            _tecsoDb.SaveChanges();

            TecsoLogger.LogMessage("Se creo una nueva Materia", Tipos.NivelLog.Informativo);

            return Ok();
        }
        #endregion

        #region Actualizar una Materia
        // PUT: api/Materia/5
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public ActionResult Put(int id, [FromBody] MateriaDto materiaDto)
        {
            if (id == 0)
                return BadRequest();
            var materia = _tecsoDb.Materias.FirstOrDefault(x => x.Id == id);
            if (materia == null)
                return NotFound();
            _mapper.Map(materiaDto, materia);
            materia.Id = id;

            _tecsoDb.Materias.Update(materia);
            _tecsoDb.SaveChanges();

            TecsoLogger.LogMessage("Se actualizo la información de una Materia", Tipos.NivelLog.Informativo);

            return Ok();
        }

        #endregion

        #region Eliminar una Materia
        // DELETE: api/Materia/5
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public ActionResult Delete(int id)
        {
            if (id == 0)
                return BadRequest();
            var materia = _tecsoDb.Materias.FirstOrDefault(x => x.Id == id);

            if (materia == null)
                return NotFound();

            if (materia.Cursos.Count > 0)
                return BadRequest("La Materia tiene Cursos Asociados");

            _tecsoDb.Materias.Remove(materia);

            _tecsoDb.SaveChanges();

            TecsoLogger.LogMessage("Se elimino una Materia", Tipos.NivelLog.Informativo);

            return Ok("Materia eliminada");
        }
        #endregion

        #endregion
    }
}
