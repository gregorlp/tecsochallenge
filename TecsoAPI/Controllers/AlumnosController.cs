﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using TecsoContract.Alumno;
using TecsoData;
using TecsoInfrastructure.Log;
using TecsoModel.Model;

namespace TecsoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlumnosController : ControllerBase
    {
        private readonly TecsoDbContext _tecsoDb;
        private readonly IMapper _mapper;


        public AlumnosController(TecsoDbContext tecsoDb, IMapper mapper)
        {
            _tecsoDb = tecsoDb;
            _mapper = mapper;
        }

        #region Methods

        #region Todos los Alumnos
        // GET: api/Alumno
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public ActionResult<IList<AlumnoDto>> Get()
        {
            var alumnos = _tecsoDb.Alumnos.ToList();

            return _mapper.Map(alumnos, new List<AlumnoDto>());
        }

        #endregion

        #region Un Alumno especifico
        // GET: api/Alumno/5
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public ActionResult<AlumnoDto> Get(int id)
        {
            var alumno = _tecsoDb.Alumnos.FirstOrDefault(x => x.Id == id);

            if (alumno != null)
                return Ok(_mapper.Map(alumno, new AlumnoDto()));

            return NotFound();
        }

        #endregion

        #region Create Alumno
        // POST: api/Alumno
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public ActionResult Post([FromBody] CreateOrEditAlumnoRequestDto alumnoDto)
        {
            var alumno = _tecsoDb.Alumnos.FirstOrDefault(x => x.Numero == alumnoDto.Numero);

            if (alumno != null)
            {
                TecsoLogger.LogMessage("Intento Fallido de crear alumno", Tipos.NivelLog.Warning);

                return BadRequest("Ya existe un alumno con ese número");
            }

            var newAlumno = _mapper.Map(alumnoDto, new Alumno());

            _tecsoDb.Alumnos.Add(newAlumno);

            _tecsoDb.SaveChanges();

            TecsoLogger.LogMessage("Se agrego un alumno", Tipos.NivelLog.Informativo);

            return Ok("Alumno Creado");
        }

        #endregion

        #region Actualizar un Alumno
        // PUT: api/Alumno/5
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public ActionResult Put(int id, [FromBody] CreateOrEditAlumnoRequestDto alumnoDto)
        {
            if (id == 0)
                return BadRequest();
            var alumno = _tecsoDb.Alumnos.FirstOrDefault(x => x.Id == id);
            if (alumno == null)
                return NotFound();
            _mapper.Map(alumnoDto, alumno);
            alumno.Id = id;

            _tecsoDb.Alumnos.Update(alumno);
            _tecsoDb.SaveChanges();

            TecsoLogger.LogMessage("Se actualizo un alumno", Tipos.NivelLog.Informativo);
            return Ok();
        }

        #endregion

        #region Eliminar un Alumno
        // DELETE: api/Alumno/5
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public ActionResult Delete(int id)
        {
            if (id == 0)
                return BadRequest();
            var alumno = _tecsoDb.Alumnos.FirstOrDefault(x => x.Id == id);

            if (alumno == null)
                return NotFound();

            _tecsoDb.Alumnos.Remove(alumno);
            _tecsoDb.SaveChanges();

            TecsoLogger.LogMessage("Se elimino un alumno", Tipos.NivelLog.Informativo);

            return Ok("Alumno eliminado");

        }

        #endregion

        #endregion
    }
}
