﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoModel.Model
{
    public class EstadoCursada : Entity
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}
