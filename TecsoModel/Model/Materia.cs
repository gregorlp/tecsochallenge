﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoModel.Model
{
    public class Materia : Entity
    {
        public string Nombre { get; set; }
        public bool Anual { get; set; }
        public virtual ICollection<Curso> Cursos { get; set; }
    }
}
