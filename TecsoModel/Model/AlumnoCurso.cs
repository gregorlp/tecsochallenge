﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoModel.Model
{
    public class AlumnoCurso
    {
        public int IdAlumno { get; set; }
        public virtual Alumno Alumno { get; set; }

        public int IdCurso { get; set; }

        public virtual Curso Curso { get; set; }

        public int IdEstadoCursada { get; set; }
        public virtual EstadoCursada EstadoCursada { get; set; }
    }
}
