﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoModel.Model
{
    public class Curso : Entity
    {
        public int IdMateria { get; set; }
        public virtual Materia Materia { get; set; }
        public string Año { get; set; }
        public string Semestre { get; set; }
        public virtual ICollection<AlumnoCurso> Alumnos { get; set; }
    }
}
