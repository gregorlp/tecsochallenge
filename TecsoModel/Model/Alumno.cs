﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoModel.Model
{
    public class Alumno : Entity
    {
        public string Apellido { get; set; }
        public string Nombres { get; set; }
        public string Numero { get; set; }
        public virtual ICollection<AlumnoCurso> Cursos { get; set; }
    }
}
