﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoModel
{
    public interface ITrackable
    {
        DateTime FechaCreacion { get; set; }
        int Id { get; set; }

    }
}
