﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TecsoModel
{
    public class Entity : ITrackable
    {
        public int Id { get; set; }

        public DateTime FechaCreacion { get; set; }
    }
}
