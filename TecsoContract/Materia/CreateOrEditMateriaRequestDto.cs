﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoContract.Materia
{
    public class CreateOrEditMateriaRequestDto : DtoEntity
    {
        public string Nombre { get; set; }
        public bool Anual { get; set; }

        public class AlumnoCreateValidator : AbstractValidator<CreateOrEditMateriaRequestDto>
        {
            public AlumnoCreateValidator()
            {
                RuleFor(x => x.Nombre).NotEmpty();
                RuleFor(x => x.Anual).NotEmpty();
            }
        }
    }
}
