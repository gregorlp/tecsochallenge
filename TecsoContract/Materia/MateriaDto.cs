﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoContract.Materia
{
    public class MateriaDto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool Anual { get; set; }
    }
}
