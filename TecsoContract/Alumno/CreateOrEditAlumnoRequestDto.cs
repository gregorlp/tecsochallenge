﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoContract.Alumno
{
    public class CreateOrEditAlumnoRequestDto : DtoEntity
    {
        public string Apellido { get; set; }
        public string Nombres { get; set; }
        public string Numero { get; set; }

        public class AlumnoCreateOrEditValidator : AbstractValidator<CreateOrEditAlumnoRequestDto>
        {
            public AlumnoCreateOrEditValidator()
            {
                RuleFor(x => x.Apellido).NotEmpty();
                RuleFor(x => x.Nombres).NotEmpty();
                RuleFor(x => x.Numero).NotEmpty();
            }
        }
    }
}
