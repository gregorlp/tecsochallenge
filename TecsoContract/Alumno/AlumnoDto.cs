﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoContract.Alumno
{
    public class AlumnoDto
    {
        public int Id { get; set; }
        public string Apellido { get; set; }
        public string Nombres { get; set; }
        public string Numero { get; set; }
    }
}
