﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoContract.Inscripcion
{
    public class EstadoCursadaDto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}
