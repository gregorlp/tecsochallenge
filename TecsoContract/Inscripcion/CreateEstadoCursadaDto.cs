﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoContract.Inscripcion
{
    public class CreateEstadoCursadaRequestDto : DtoEntity
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

        public class CreateEstadoCursadaValidator : AbstractValidator<CreateEstadoCursadaRequestDto>
        {
            public CreateEstadoCursadaValidator()
            {
                RuleFor(x => x.Nombre).NotEmpty();
            }
        }
    }
}
