﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoContract.Inscripcion
{
    public class CreateInscripcionRequestDto
    {
        public int IdCurso { get; set; }
        public int IdAlumno { get; set; }

        public class InscripcionCreateValidator : AbstractValidator<CreateInscripcionRequestDto>
        {
            public InscripcionCreateValidator()
            {
                RuleFor(x => x.IdCurso).NotEmpty();
                RuleFor(x => x.IdAlumno).NotEmpty();
            }
        }
    }
}
