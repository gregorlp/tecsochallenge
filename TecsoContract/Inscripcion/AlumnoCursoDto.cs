﻿using System;
using System.Collections.Generic;
using System.Text;
using TecsoContract.Curso;

namespace TecsoContract.Alumno
{
    public class AlumnoCursoDto
    {
        public int IdAlumno { get; set; }
        public AlumnoDto Alumno { get; set; }

        public int IdCurso { get; set; }

        public string Materia { get; set; }
        public string Año { get; set; }
        public int IdEstadoCursada { get; set; }
        public string EstadoCursada { get; set; }
    }
}
