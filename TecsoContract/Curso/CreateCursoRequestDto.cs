﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoContract.Curso
{
    public class CreateCursoRequestDto
    {
        public int IdMateria { get; set; }
        public string Año { get; set; }
        public string Semestre { get; set; }

        public class CursoCreateOrEditValidator : AbstractValidator<CreateCursoRequestDto>
        {
            public CursoCreateOrEditValidator()
            {
                RuleFor(x => x.IdMateria).NotEmpty();
                RuleFor(x => x.Año).NotEmpty();
            }
        }
    }
}
