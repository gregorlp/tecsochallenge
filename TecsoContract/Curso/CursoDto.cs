﻿using System;
using System.Collections.Generic;
using System.Text;
using TecsoContract.Alumno;
using TecsoContract.Materia;

namespace TecsoContract.Curso
{
    public class CursoDto :DtoEntity
    {
        public int IdMateria { get; set; }
        public string Materia { get; set; }
        public string Año { get; set; }
        public string Semestre { get; set; }
        public ICollection<AlumnoDto> Alumnos { get; set; }
    }
}
