﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using TecsoContract.Alumno;
using TecsoModel.Model;
using System.Linq;
using TecsoContract.Materia;
using TecsoContract.Curso;
using TecsoContract.Inscripcion;

namespace TecsoInfrastructure.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Alumno, AlumnoDto>();

            CreateMap<CreateOrEditAlumnoRequestDto, Alumno>().ForMember(x => x.Cursos, opt => opt.Ignore());

            CreateMap<Materia, MateriaDto>().ReverseMap().ForMember(x => x.Cursos, opt => opt.Ignore());

            CreateMap<Curso, CursoDto>().ForMember(x => x.Alumnos, opt => opt.MapFrom(x => (x.Alumnos.Select(x => x.Alumno)))).ForMember(x => x.Materia, opt => opt.MapFrom(x => x.Materia.Nombre));

            CreateMap<CreateCursoRequestDto, Curso>();

            CreateMap<AlumnoCurso, AlumnoCursoDto>().ForMember(x => x.EstadoCursada, opt => opt.MapFrom(x => x.EstadoCursada.Nombre)).ForMember(x => x.Materia, opt => opt.MapFrom(x => x.Curso.Materia.Nombre)).ForMember(x => x.Año, opt => opt.MapFrom(x => x.Curso.Año)).ReverseMap().IgnoreAllVirtual();

            CreateMap<CreateOrEditMateriaRequestDto, Materia>().ForMember(x => x.Cursos, opt => opt.Ignore());

            CreateMap<CreateInscripcionRequestDto, AlumnoCurso>().ForMember(x => x.IdEstadoCursada, opt => opt.Ignore()).IgnoreAllVirtual();

            CreateMap<EstadoCursada, EstadoCursadaDto>();
        }
    }
}
