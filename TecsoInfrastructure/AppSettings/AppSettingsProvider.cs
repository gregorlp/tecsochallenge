﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoInfrastructure.AppSettings
{
    public static class AppSettingsProvider
    {
        public static string SqliteConnection { get; set; }
        public static string PathLogFolder{ get; set; }
    }
}
