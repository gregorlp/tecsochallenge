﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TecsoInfrastructure.Log;

namespace TecsoInfrastructure.ExceptionHandling
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            string logMessage = null;
            string innerMessage = null;
            var code = 0;

            switch (exception)
            {
                case DbUpdateException re:
                    innerMessage = re.InnerException != null ? re.InnerException.Message : "";
                    context.Response.StatusCode = code = (int)HttpStatusCode.InternalServerError;
                    logMessage = "Sqlite Error ";
                    if (re.InnerException != null)
                        logMessage += "Sqlite Exception" + ((SQLiteException)re.InnerException).ResultCode;
                    logMessage += innerMessage;
                    break;
                case Exception e:
                    innerMessage = e.InnerException != null ? e.InnerException.Message : "";
                    logMessage = string.IsNullOrWhiteSpace(e.Message) ? "Error" : e.Message + innerMessage;
                    code = (int)HttpStatusCode.InternalServerError;
                    context.Response.StatusCode = code = (int)HttpStatusCode.InternalServerError;
                    break;
            }

            context.Response.ContentType = "application/json";

            TecsoLogger.LogMessage(logMessage, Tipos.NivelLog.Error);

            var result = JsonConvert.SerializeObject(new
            {
                Message = logMessage,
                Code = code
            });

            await context.Response.WriteAsync(result);
        }
    }
}
