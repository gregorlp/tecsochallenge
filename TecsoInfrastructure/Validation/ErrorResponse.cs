﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoInfrastructure.Validation
{
    public class ErrorResponse
    {
        public IList<ErrorModel> Errors { get; set; }
    }
}
