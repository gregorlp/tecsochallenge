﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoInfrastructure.Validation
{
    public class ErrorModel
    {
        public string FieldName { get; set; } // actual name of the field

        public string Message { get; set; } // user friendly error message
    }
}
