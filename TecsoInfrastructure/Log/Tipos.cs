﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoInfrastructure.Log
{
    public static class Tipos
    {
        public enum NivelLog
        {
            Informativo = 1,
            Error = 2,
            Warning = 3
        }
    }
}
