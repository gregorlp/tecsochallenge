﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoInfrastructure.Log
{
    using Microsoft.Extensions.Options;
    using System;
    using System.Linq;
    using System.Text;
    using TecsoInfrastructure.AppSettings;

    public static class TecsoLogger
    {
        private static bool _logToFile;
        private static bool _logToConsole;
        private static bool _logMessage;
        private static bool _logWarning;
        private static bool _logError;
        private static bool LogToDatabase;
        private static bool _initialized;
        public static void Configurate(bool logToFile, bool logToConsole, bool logToDatabase, bool logMessage,
        bool logWarning, bool logError)
        {
            _logError = logError;
            _logMessage = logMessage;
            _logWarning = logWarning;
            LogToDatabase = logToDatabase;
            _logToFile = logToFile;
            _logToConsole = logToConsole;
            _initialized = true;
        }
        public static void LogMessage(string message, Tipos.NivelLog? nivelLog)
        {
            message.Trim();

            if (!_initialized)
                throw new Exception("Debe Configurar TecsoLogger");

            if (message == null || message.Length == 0)
            {
                return;
            }
            if (!_logToConsole && !_logToFile && !LogToDatabase)
            {
                throw new Exception("Código incorrecto");
            }
            if (nivelLog == null)
            {
                throw new Exception("Especificar tipo de mensaje");
            }

            LogInDatabase(message, nivelLog.Value);

            LogInConsole(message, nivelLog.Value);

            LogInFile(message, nivelLog.Value);
        }

        private static void LogInDatabase(string message, Tipos.NivelLog nivelLog)
        {
            string dataSource = AppSettingsProvider.SqliteConnection;

            System.Data.SQLite.SQLiteConnection connection = new
System.Data.SQLite.SQLiteConnection(dataSource);
            connection.Open();

            System.Data.SQLite.SQLiteCommand command = new
            System.Data.SQLite.SQLiteCommand("Insert into Log(Message,FechaCreacion,Codigo) Values('" + message + "','" + DateTime.UtcNow.ToString() + "', " + ((int)nivelLog).ToString() +
            ")", connection);

            command.ExecuteNonQuery();

            connection.Close();
        }
        private static void LogInConsole(string message, Tipos.NivelLog nivelLog)
        {

            Console.WriteLine(DateTime.Now.ToShortDateString() + "--" + message + "--" + nivelLog.ToString());
        }
        private static void LogInFile(string message, Tipos.NivelLog nivelLog)
        {
            string l = "";

            var nameArchivo = String.Join("Archivo_Log", DateTime.Now.ToShortDateString().Replace("/", "-"), ".txt");

            if
            (System.IO.File.Exists(AppSettingsProvider.PathLogFolder +
            nameArchivo))
            {
                l =
                System.IO.File.ReadAllText(AppSettingsProvider.PathLogFolder + nameArchivo);
            }

            l = l + DateTime.Now.ToShortDateString() + ":" + DateTime.Now.ToShortTimeString() + "--" + message + "\n";

            System.IO.File.WriteAllText(String.Concat(AppSettingsProvider.PathLogFolder, nameArchivo), l);
        }
    }
}