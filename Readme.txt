﻿La Aplicación es una API, que brinda una interfaz de consulta a través de Swagger => https://swagger.io/

Se debe correr la aplicación con el profile de TecsoAPI. Se podrá interactuar con la API, a través de la 
Interfaz provista por Swagger. No se requiere instalación de SQl Server ya que se utiliza un Sqlite que 
ya viene con el proyecto.

Tecnologías Utilizadas
.Net Core 3.1
AutoMapper
FluentValidation
Swashbuckle
SqLite
EntityFramework

Desarrollado con Visual Studio 2019

La API viene con :
- Dos Estados de Cursada precargados => "En Curso" y "Aprobado"
- Dos Alumnos Precargados de prueba
- Dos Cursos de diferentes Materias Precargados de prueba
- Cuatro Inscripciones, ambos alumnos inscriptos en las dos materias disponibles
- Un Alumno aprobado en la Materia 1
En el archivo TecnicaRespuestas.doc se encuentran las respuestas a las consignas planteadas

Si se quiere consultar la base de datos con un IDE como sqldbx desde un windows:

- Descargar Sqlite Precompiled binaries for windows 
	=> https://www.sqlite.org/2020/sqlite-dll-win32-x86-3310100.zip

- Extraer y pegar la DLL en  C:\Windows\SysWOW64

- Luego registrar la dll con tecla windows + r => regsvr32 "C:\Windows\SysWOW64\sqlite3.dll"

- Descargar Sqlite ODBC Driver => http://www.ch-werner.de/sqliteodbc/sqliteodbc_dl.exe => Instalar el exe

- Agregar Sqlite a ODBC 32 del Sistema 

- Para configurar la base de datos mediante ODBC:
		=> Presionar Tecla Inicio 
		=> Buscar Odbc Data Source (32 bits)
		=> ejecutar con permisos de administrador 
		=> DNS de Sistema 
		=> SqLite3 DataSource 
		=> Agregar 
		=> seleccionar driver Sqlite 3 Odbc Driver 
		=> Buscar archivo de Base de Datos llamado tecso.db dentro del proyecto 
					("tecso_project\tecsochallenge\TecsoData\tecso.db")