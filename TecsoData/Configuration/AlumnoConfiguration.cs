﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TecsoModel.Model;

namespace TecsoData.Configuration
{
    internal class AlumnoConfiguration : IEntityTypeConfiguration<Alumno>
    {
        public void Configure(EntityTypeBuilder<Alumno> builder)
        {
            builder.HasKey(e => e.Id);

            builder.ToTable("Alumno");
        }
    }
}
