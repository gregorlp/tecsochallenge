﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TecsoModel.Model;

namespace TecsoData.Configuration
{
    internal class EstadoCursadaConfiguration : IEntityTypeConfiguration<EstadoCursada>
    {
        public void Configure(EntityTypeBuilder<EstadoCursada> builder)
        {
            builder.HasKey(e => e.Id);

            builder.ToTable("EstadoCursada");
        }
    }
}
