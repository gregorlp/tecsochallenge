﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TecsoModel.Model;

namespace TecsoData.Configuration
{
    internal class CursoConfiguration : IEntityTypeConfiguration<Curso>
    {
        public void Configure(EntityTypeBuilder<Curso> builder)
        {
            builder.HasKey(e => e.Id);

            builder.HasOne(x => x.Materia).WithMany(x => x.Cursos).HasForeignKey(x => x.IdMateria).OnDelete(DeleteBehavior.Restrict);

            builder.ToTable("Curso");
        }
    }
}
