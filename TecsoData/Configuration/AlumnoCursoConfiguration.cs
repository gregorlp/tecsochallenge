﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TecsoModel.Model;

namespace TecsoData.Configuration
{
    internal class AlumnoCursoConfiguration : IEntityTypeConfiguration<AlumnoCurso>
    {
        public void Configure(EntityTypeBuilder<AlumnoCurso> builder)
        {
            builder.HasKey(e => new { e.IdAlumno, e.IdCurso });

            builder.HasIndex(d => d.IdAlumno);
            builder.HasOne(d => d.Alumno)
                .WithMany(p => p.Cursos)
                .HasForeignKey(d => d.IdAlumno)
                .HasConstraintName("FK_AlumnoCurso_Alumno")
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasIndex(d => d.IdCurso);
            builder.HasOne(d => d.Curso)
                .WithMany(p => p.Alumnos)
                .HasForeignKey(d => d.IdCurso)
                .HasConstraintName("FK_AlumnoCurso_Curso")
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.EstadoCursada).WithMany().HasForeignKey(x => x.IdEstadoCursada);
            builder.ToTable("AlumnoCurso");
        }
    }
}
