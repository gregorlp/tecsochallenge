﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace TecsoData
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<TecsoDbContext>
    {
        public TecsoDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .Build();

            var builder = new DbContextOptionsBuilder<TecsoDbContext>();

            return new TecsoDbContext(builder.Options);
        }
    }
}
