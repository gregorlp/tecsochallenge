﻿using Microsoft.EntityFrameworkCore;
using System;
using TecsoData.Extension;
using TecsoModel;
using TecsoModel.Model;

namespace TecsoData
{
    public class TecsoDbContext : DbContext
    {
        public TecsoDbContext(DbContextOptions<TecsoDbContext> options)
    : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies().UseSqlite("Data Source=..\\TecsoData\\tecso.db");
        }

        public DbSet<Alumno> Alumnos { get; set; }
        public DbSet<Curso> Cursos { get; set; }
        public DbSet<Materia> Materias { get; set; }
        public DbSet<AlumnoCurso> Inscripciones { get; set; }
        public DbSet<EstadoCursada> EstadosCursada { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyAllConfigurations();
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            OnBeforeSaving();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        private void OnBeforeSaving()
        {
            var entries = ChangeTracker.Entries();
            foreach (var entry in entries)
                if (entry.Entity is ITrackable trackable)
                {
                    var now = DateTime.Now;

                    switch (entry.State)
                    {
                        case EntityState.Added:
                            trackable.FechaCreacion = now;
                            break;
                    }
                }
        }
    }
}
